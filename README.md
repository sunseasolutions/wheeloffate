**Wheel Of Fate**
=====================

This is an example ASP.NET Application to show how I would start building a working website to select team members for the BAU (Business As Usual) Support Wheel Of fate.
This is not finished but should be enough to show the direction and level of skills.

[ Version history ](CHANGELOG.md)

## Live URL
[http://fate.sunseasolutions.com](http://fate.sunseasolutions.com)


The solution is divided into several projects
---------------------------------------------
### Database
SQL Server Database Project (Right click and publish to update)

### DataAccess
for accessing the database. (Entity Framework)

### Business
the business logic

### Domain
the business model

### DTO
Data transfer objects for passing data between layers

### Tests
Unit test project

### Web
The UI bit

 