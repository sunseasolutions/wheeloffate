﻿using System;
using System.Collections.Generic;
using WheelOfFate.Domain;
using WheelOfFate.Dto;

namespace WheelOfFate.Business
{
    public interface IWheelOfFateService
    {
        IEnumerable<Engineer> GetLuckyWinners();
        IEnumerable<SupportLogDto> GetSupportLog();
        void AddNextWinners(int morningEngineerId, int afternoonEngineerId, DateTime shiftDate);
        void RemoveLastShift();
    }
}
