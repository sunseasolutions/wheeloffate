﻿using System;
using System.Collections.Generic;
using System.Linq;
using WheelOfFate.DataAccess;
using WheelOfFate.Domain;
using WheelOfFate.Dto;

namespace WheelOfFate.Business
{
    public class WheelOfFateService : IWheelOfFateService
    {
        #region Properties

        private readonly IEngineerDataAccess _engineerRepo;

        #endregion

        #region Constructor(s)

        /// <summary>
        /// Constructor
        /// </summary>
        public WheelOfFateService(IEngineerDataAccess engineerRepo)
        {
            _engineerRepo = engineerRepo;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get the lucky winners
        /// </summary>
        public IEnumerable<Engineer> GetLuckyWinners()
        {
            // Initialize return value
            var returnEngineers = new List<Engineer>();

            // Get all engineers
            var allEngineers = _engineerRepo.GetAll().ToList();
            
            // Get last 2 weeks support logs
            var last2Weeks = _engineerRepo.GetLast2Weeks().ToList();
            
            // Get those that didn't work the last shift
            var allEngineersExceptLastShift = GetEngineersThatDidNotDoTheLastShift(allEngineers, last2Weeks).ToList();

            // Get possible candidates that have not yet done 2 shifts in the last 2 weeks
            var possibleEngineers = GetEngineersThatHaveNotDone2Shifts(allEngineersExceptLastShift, last2Weeks).ToList();

            var rnd = new Random();
            if (!possibleEngineers.Any())
            {
                // If no results then just get any except the last shift
                returnEngineers = allEngineersExceptLastShift.OrderBy(engineer => rnd.Next()).Take(2).ToList();
            }
            else if(possibleEngineers.Count == 1)
            {
                // If 1 result then add
                var non2ShiftEngineer = possibleEngineers[0];
                returnEngineers.Add(non2ShiftEngineer);

                // Add another as long as it wasn't the last shift
                var nextEngineer = allEngineersExceptLastShift.Where(p=>p.EngineerId != non2ShiftEngineer.EngineerId).OrderBy(engineer => rnd.Next()).Take(1).First();
                returnEngineers.Add(nextEngineer);
            }
            else
            {
                // Engineers that have not yet done 2 shifts
                returnEngineers = possibleEngineers.OrderBy(engineer => rnd.Next()).Take(2).ToList();
            }
            
            return returnEngineers;
        }

        /// <summary>
        /// Get Last 2 weeks from the support log
        /// </summary>
        public IEnumerable<SupportLogDto> GetSupportLog()
        {
            return _engineerRepo.GetLast2Weeks();
        }

        /// <summary>
        /// Add the next 2 winners to the support log
        /// </summary>
        public void AddNextWinners(int morningEngineerId, int afternoonEngineerId, DateTime shiftDate)
        {
            // Create engineer log
            var log = new SupportLog()
            {
                LogDate = shiftDate,
                EngineerIdMorning = morningEngineerId,
                EngineerIdAfternoon = afternoonEngineerId,
                DateAdded = DateTime.Now
            };
            
            // Add log to repo
            _engineerRepo.InsertLog(log);
        }

        /// <summary>
        /// Add the next 2 winners to the support log
        /// </summary>
        public void RemoveLastShift()
        {
            // Get last 2 weeks support logs
            var last2Weeks = _engineerRepo.GetLast2Weeks().ToList();

            // Get last shift date
            var lastShiftDate = GetLastShiftDate(last2Weeks);

            // Remove last log
             _engineerRepo.DeleteLogEntryById(lastShiftDate);
        }

        #endregion

        #region Private Methods

        private static IEnumerable<Engineer> GetEngineersThatHaveNotDone2Shifts(IEnumerable<Engineer> allEngineers, IEnumerable<SupportLogDto> last2Weeks)
        {
            // Find those that have already done 2 shifts
            var supportLogDtos = last2Weeks as SupportLogDto[] ?? last2Weeks.ToArray();
            var engineerIds = supportLogDtos.Select(l => l.EngineerIdMorning).ToList();
            engineerIds.AddRange(supportLogDtos.Select(l => l.EngineerIdAfternoon));
            var done2Shifts = engineerIds.GroupBy(id => id).Where(grp => grp.Count() > 1);

            return allEngineers.Where(e => done2Shifts.All(i => i.Key != e.EngineerId));
        }

        private static IEnumerable<Engineer> GetEngineersThatDidNotDoTheLastShift(IEnumerable<Engineer> possibleEngineers, IList<SupportLogDto> last2Weeks)
        {
            // Find the last shift date
            var lastShiftDate = GetLastShiftDate(last2Weeks);

            // Get last engineer ids
            var lastEngineerIds = last2Weeks.Where(l => l.LogDate == lastShiftDate).Select(l => l.EngineerIdMorning).ToList();
            lastEngineerIds.Add(last2Weeks.Where(l => l.LogDate == lastShiftDate).Select(l => l.EngineerIdAfternoon).FirstOrDefault());

            // return those that did not work last shift
            return possibleEngineers.Where(e => lastEngineerIds.All(i => i != e.EngineerId));
        }

        private static DateTime GetLastShiftDate(IEnumerable<SupportLogDto> last2Weeks)
        {
            // Find the last shift date
            var lastShift = last2Weeks.OrderByDescending(t => t.LogDate).FirstOrDefault();
            return lastShift?.LogDate.Date ?? DateTime.Now;
        }
        
        #endregion
    }
}
