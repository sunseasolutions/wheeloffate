﻿using AutoMapper;
using WheelOfFate.Domain;
using WheelOfFate.Dto;

namespace WheelOfFate.DataAccess.Configuration
{
    public static class AutoMapperDataConfig
    {
        public static void Configure()
        {
            ConfigurePropertyMappings();
        }

        private static void ConfigurePropertyMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<SupportLog, SupportLogDto>()
                    .ForMember(dto => dto.EngineerNameMorning, opt => opt.MapFrom(e => e.EngineerMorning.Name))
                    .ForMember(dto => dto.EngineerNameAfternoon, opt => opt.MapFrom(e => e.EngineerAfternoon.Name));
            });
        }
    }
}