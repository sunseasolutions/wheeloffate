﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WheelOfFate.Domain;

namespace WheelOfFate.DataAccess.Configuration
{
    public class SupportLogConfig : EntityTypeConfiguration<SupportLog>
    {
        public SupportLogConfig()
        {
            // Set primary key
            HasKey(l => new { l.LogDate });

            // Set foreign keys
            HasRequired(l => l.EngineerMorning).WithMany().HasForeignKey(e => e.EngineerIdMorning);
            HasRequired(l => l.EngineerAfternoon).WithMany().HasForeignKey(e => e.EngineerIdAfternoon);
        }
    }
}
