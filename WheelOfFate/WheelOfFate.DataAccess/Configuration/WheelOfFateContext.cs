﻿using System.Data.Entity;
using WheelOfFate.Domain;

namespace WheelOfFate.DataAccess.Configuration
{
    public class WheelOfFateContext : DbContext
    {
        public WheelOfFateContext() : base("WheelOfFateConnectionString")
        {
            // Disable initializer as we'll use the SQL DB project
            Database.SetInitializer<WheelOfFateContext>(null);
        }

        public DbSet<Engineer> Engineers { get; set; }
        public DbSet<SupportLog> EngineerLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SupportLogConfig());
            
            base.OnModelCreating(modelBuilder);
        }

    }

}
