﻿using System.Data.Entity;
using System.Linq;
using WheelOfFate.DataAccess.Configuration;

namespace WheelOfFate.DataAccess
{
    public class DataAccess<T> : IDataAccess<T> where T : class
    {
        private WheelOfFateContext _dbContext;

        protected IDbFactory DbFactory
        {
            get;
        }

        protected WheelOfFateContext DbContext => _dbContext ?? (_dbContext = DbFactory.Init());
        protected IDbSet<T> Entities => _dbContext.Set<T>();

        public DataAccess(IDbFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public IQueryable<T> GetAll()
        {
            return DbContext.Set<T>();
        }

        public void Insert(T entity)
        {
            Entities.Add(entity);
            _dbContext.SaveChanges();
        }

        public void Delete(T entity)
        {
            Entities.Remove(entity);
            _dbContext.SaveChanges();
        }

    }
}
