﻿using WheelOfFate.DataAccess.Configuration;

namespace WheelOfFate.DataAccess
{
    public class DbFactory : IDbFactory
    {
        private WheelOfFateContext _dbContext;

        public WheelOfFateContext Init()
        {
            return _dbContext ?? (_dbContext = new WheelOfFateContext());
        }
    }
}