﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper.QueryableExtensions;
using WheelOfFate.Domain;
using WheelOfFate.Dto;

namespace WheelOfFate.DataAccess
{
    public class EngineerDataAccess : DataAccess<Engineer>, IEngineerDataAccess
    {
        public EngineerDataAccess(IDbFactory dbFactory)
            : base(dbFactory)
        {
        }

        public void DeleteLogEntryById(DateTime date)
        {
            // return all items in last 2 weeks
            var log = DbContext
                .Set<SupportLog>()
                .FirstOrDefault(e => e.LogDate == date);

            // Delete
            if (log == null) return;
            DbContext.Set<SupportLog>().Remove(log);
            DbContext.SaveChanges();
        }

        public IEnumerable<SupportLogDto> GetLast2Weeks()
        {
            // return last 2 weeks data
            return DbContext.Set<SupportLog>()
                .OrderByDescending(d => d.LogDate)
                .Take(14)
                .ProjectTo<SupportLogDto>();
        }

        public void InsertLog(SupportLog engineerLog)
        {
            DbContext.Set<SupportLog>().Add(engineerLog);
            DbContext.SaveChanges();
        }
    }
}
