﻿using System.Linq;

namespace WheelOfFate.DataAccess
{
    public interface IDataAccess<T> where T : class
    {
        IQueryable<T> GetAll();
        void Insert(T entity);
        void Delete(T entity);
    }
}
