﻿using WheelOfFate.DataAccess.Configuration;

namespace WheelOfFate.DataAccess
{
    public interface IDbFactory
    {
        WheelOfFateContext Init();
    }
}
