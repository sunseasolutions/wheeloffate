﻿using System;
using System.Collections.Generic;
using WheelOfFate.Domain;
using WheelOfFate.Dto;

namespace WheelOfFate.DataAccess
{
    public interface IEngineerDataAccess : IDataAccess<Engineer>
    {
        IEnumerable<SupportLogDto> GetLast2Weeks();
        void InsertLog(SupportLog engineerLog);
        void DeleteLogEntryById(DateTime date);
    }
}
