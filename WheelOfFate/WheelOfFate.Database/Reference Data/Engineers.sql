﻿-- Engineer Data
SET IDENTITY_INSERT [dbo].[Engineers] ON;
GO

MERGE INTO [dbo].[Engineers] AS Target
USING (VALUES
    (1, 'Fred')
	, (2, 'Bob')
	, (3, 'Bill')
	, (4, 'Irene')
	, (5, 'Bert')
	, (6, 'Liz')
	, (7, 'Victoria')
	, (8, 'Sarah')
	, (9, 'Joanne')
	, (10, 'Phillip')
) AS Source (
	[EngineerId]
	, [Name]
)
ON Target.[EngineerId] = Source.[EngineerId]
-- Update matched rows
WHEN MATCHED THEN
UPDATE SET 
	[Name] = Source.[Name]

-- Insert new rows
WHEN NOT MATCHED BY TARGET THEN
INSERT (
	[EngineerId]
	, [Name]
)
VALUES (
	[EngineerId]
	, [Name]
)

-- Delete rows that are in the target but not the source
WHEN NOT MATCHED BY SOURCE THEN
DELETE;
GO

SET IDENTITY_INSERT [dbo].[Engineers] OFF;
GO