﻿CREATE TABLE [dbo].[SupportLogs]
(
	[LogDate] DATETIME NOT NULL PRIMARY KEY,
    [EngineerIdMorning] INT NOT NULL, 
    [EngineerIdAfternoon] INT NOT NULL, 
    [DateAdded] DATETIME NOT NULL DEFAULT GETDATE(),
	CONSTRAINT [FK_EngineerMorning_Engineer] FOREIGN KEY ([EngineerIdMorning]) REFERENCES [Engineers]([EngineerId]),
	CONSTRAINT [FK_EngineerAfternoon_Engineer] FOREIGN KEY ([EngineerIdAfternoon]) REFERENCES [Engineers]([EngineerId])
)
