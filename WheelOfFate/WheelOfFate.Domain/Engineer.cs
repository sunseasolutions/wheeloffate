﻿using System;

namespace WheelOfFate.Domain
{
    public class Engineer
    {
        public int EngineerId { get; set; }
        public string Name { get; set; }
        public DateTime DateAdded { get; set; }
    }
}
