﻿using System;

namespace WheelOfFate.Domain
{
    public class SupportLog
    {
        public DateTime LogDate { get; set; }
        public int EngineerIdMorning { get; set; }
        public int EngineerIdAfternoon { get; set; }
        public DateTime DateAdded { get; set; }

        public virtual Engineer EngineerMorning { get; set; }
        public virtual Engineer EngineerAfternoon { get; set; }
    }
}
