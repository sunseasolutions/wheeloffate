﻿using System;

namespace WheelOfFate.Dto
{
    public class SupportLogDto
    {
        public int EngineerIdMorning { get; set; }
        public int EngineerIdAfternoon { get; set; }
        public DateTime LogDate { get; set; }
        public string EngineerNameMorning { get; set; }
        public string EngineerNameAfternoon { get; set; }
    }
}
