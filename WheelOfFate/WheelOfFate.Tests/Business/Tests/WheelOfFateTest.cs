﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using WheelOfFate.Business;
using WheelOfFate.DataAccess;
using WheelOfFate.Domain;
using WheelOfFate.Dto;
using WheelOfFate.Tests.MockObjects;

namespace WheelOfFate.Tests.Business.Tests
{
    [TestClass]
    public class WheelOfFateTest
    {
        private WheelOfFateService _wheelOfFateService;
        private IList<SupportLogDto> _supportLog;
        private IQueryable<Engineer> _mockEngineers;

        [TestInitialize]
        public void Initialize()
        {
            // Engineer List
            _mockEngineers = EngineerListMock.EngineerList;

            // Support Log
            _supportLog = SupportListMock.SupportListDtos;
        }

        /// <summary>
        /// Maybe split this into more managable chunks
        /// </summary>
        [TestMethod]
        public void TestLuckyWinners()
        {
            // Get mock engineers list
            var mockDb = new Mock<IEngineerDataAccess>();
            mockDb.Setup(x => x.GetAll()).Returns(_mockEngineers);
            mockDb.Setup(x => x.GetLast2Weeks()).Returns(_supportLog);

            // Get mock support log
            var mockService = new Mock<IWheelOfFateService>();
            mockService.Setup(x => x.GetSupportLog()).Returns(_supportLog);

            // Set up service
            _wheelOfFateService = new WheelOfFateService(mockDb.Object);
            
            // Reckon 100 times is enough to test
            for (var i = 0; i < 100; i++)
            {
                // Get winners
                var winningEngineers = _wheelOfFateService.GetLuckyWinners().ToList();

                // Test engineers did not work the last shift
                CollectionAssert.DoesNotContain(winningEngineers, _mockEngineers.First());
                CollectionAssert.DoesNotContain(winningEngineers, _mockEngineers.Last());

                // Check they are different engineers
                Assert.AreNotSame(winningEngineers[0], winningEngineers[1]);

                // Check there are always 2 returned
                Assert.IsTrue(winningEngineers.Count == 2);
            }
        }

        
        [TestMethod]
        public void TestEngineerDidOneWholeDayIn2WeekPeriod()
        {
            throw new NotImplementedException();
        }
    }
}
