﻿using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace WheelOfFate.Tests.DataAccess.Helpers
{
    public static class EfHelpers
    {
        public static Mock<DbSet<T>> AsMockDbSet<T>(IList<T> data, bool callBase = true) where T : class
        {
            var mockSet = new Mock<DbSet<T>>();
            var queryable = data.AsQueryable();

            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(queryable.GetEnumerator());


            mockSet.As<IDbSet<T>>().Setup(m => m.Add(It.IsAny<T>())).Returns<T>(i => { data.Add(i); return i; });
            mockSet.As<IDbSet<T>>().Setup(m => m.Remove(It.IsAny<T>())).Returns<T>(i => { data.Remove(i); return i; });
            //if (find != null) mock.As<IDbSet<TEntity>>().Setup(m => m.Find(It.IsAny<object[]>())).Returns(find);
            

            mockSet.CallBase = callBase;

            return mockSet;
        }
    }
}
