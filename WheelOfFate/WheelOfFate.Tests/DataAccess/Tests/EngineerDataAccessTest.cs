﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WheelOfFate.Domain;
using Moq;
using WheelOfFate.DataAccess;
using WheelOfFate.DataAccess.Configuration;
using WheelOfFate.Tests.DataAccess.Helpers;
using WheelOfFate.Tests.MockObjects;
using System.Linq;
using AutoMapper;
using WheelOfFate.Dto;

namespace WheelOfFate.Tests.DataAccess.Tests
{
    [TestClass]
    public class EngineerDataAccessTest
    {
        private Mock<DbSet<Engineer>> _mockEngineerDbSet;
        private Mock<WheelOfFateContext> _mockContext;
        private EngineerDataAccess _engineerRepository;
        private List<Engineer> _engineerList;
        
        [TestInitialize]
        public void TestInitialize()
        {
            // Get engineer list for the test            
            _engineerList = EngineerListMock.EngineerList.ToList();
            
            // Use helper so we can mock DbSet
            _mockEngineerDbSet = EfHelpers.AsMockDbSet(_engineerList);
            
            // Mock the DbContext
            _mockContext = new Mock<WheelOfFateContext>();
            var mockDbFactory = new Mock<IDbFactory>();
            _mockContext.Setup(m => m.Set<Engineer>()).Returns(_mockEngineerDbSet.Object);
            mockDbFactory.Setup(m => m.Init()).Returns(_mockContext.Object);


            _engineerRepository = new EngineerDataAccess(mockDbFactory.Object);
        }

        [TestMethod]
        public void GetLast2Weeks_ReturnsList()
        {
            // Set up
            var mockSupportLogDbSet = EfHelpers.AsMockDbSet(SupportListMock.SupportList);
            _mockContext.Setup(m => m.Set<SupportLog>()).Returns(mockSupportLogDbSet.Object);
            var supportLogList = _engineerRepository.GetLast2Weeks().ToList();

            // TODO: Add AutoMapper Mapping configuration
            
            // Assert
            CollectionAssert.AreEqual(supportLogList, SupportListMock.SupportListDtos.ToList());
        }

        [TestMethod]
        public void GetAll_ReturnsList()
        {
            // Set up

            // Act
            var result = _engineerRepository.GetAll();

            // Assert
            Assert.AreEqual(10, result.Count());
        }

        [TestMethod]
        public void InsertLog_InsertsLogViaContext()
        {
            // Set up
            var log = SupportListMock.SupportLogToInsert;
            var mockSupportLogDbSet = EfHelpers.AsMockDbSet(SupportListMock.SupportList);
            _mockContext.Setup(m => m.Set<SupportLog>()).Returns(mockSupportLogDbSet.Object);

            // Act
            _engineerRepository.InsertLog(log);

            // Assert
            Assert.AreEqual(2, mockSupportLogDbSet.Object.Count());
            _mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void Remove_RemovesLogViaContext()
        {
            // Set up
            var mockSupportLogDbSet = EfHelpers.AsMockDbSet(SupportListMock.SupportList);
            var id = SupportListMock.SupportList[0].LogDate;
            _mockContext.Setup(m => m.Set<SupportLog>()).Returns(mockSupportLogDbSet.Object);

            // Act
            _engineerRepository.DeleteLogEntryById(id);

            // Assert
            Assert.AreEqual(0, mockSupportLogDbSet.Object.Count());
            mockSupportLogDbSet.Verify(m => m.Remove(It.Is<SupportLog>(a => a.EngineerIdMorning == 1)), Times.Once);
            _mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }
    }
}