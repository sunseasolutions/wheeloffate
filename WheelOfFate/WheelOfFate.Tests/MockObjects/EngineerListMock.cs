﻿using System.Collections.Generic;
using System.Linq;
using WheelOfFate.Domain;

namespace WheelOfFate.Tests.MockObjects
{    
    public class EngineerListMock
    {
        // All Engineers
        public static IQueryable<Engineer> EngineerList {
            get
            {
                var mockEngineerList = new List<Engineer>
                {
                    new Engineer
                    {
                        EngineerId = 1,
                        Name = "Bill"
                    },
                    new Engineer
                    {
                        EngineerId = 2,
                        Name = "Bob"
                    },
                    new Engineer
                    {
                        EngineerId = 3,
                        Name = "Ben"
                    },
                    new Engineer
                    {
                        EngineerId = 4,
                        Name = "Bert"
                    },
                    new Engineer
                    {
                        EngineerId = 5,
                        Name = "Bessy"
                    },
                    new Engineer
                    {
                        EngineerId = 6,
                        Name = "Bonnie"
                    },
                    new Engineer
                    {
                        EngineerId = 7,
                        Name = "Berlinda"
                    },
                    new Engineer
                    {
                        EngineerId = 8,
                        Name = "Billy"
                    },
                    new Engineer
                    {
                        EngineerId = 9,
                        Name = "Bobbie"
                    },
                    new Engineer
                    {
                        EngineerId = 10,
                        Name = "Bret"
                    },
                };

                return (mockEngineerList).AsQueryable();
            }
        }
    }
}
