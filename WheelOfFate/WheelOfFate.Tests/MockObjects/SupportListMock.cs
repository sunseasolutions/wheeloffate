﻿using System;
using System.Collections.Generic;
using WheelOfFate.Domain;
using WheelOfFate.Dto;

namespace WheelOfFate.Tests.MockObjects
{    
    public class SupportListMock
    {
        public static SupportLog SupportLogToInsert => new SupportLog
        {
            LogDate = new DateTime(2000, 1, 1),
            EngineerIdMorning = 1,
            EngineerMorning = new Engineer() { DateAdded = DateTime.Now, EngineerId = 1, Name = "Bill" },
            EngineerIdAfternoon = 10,
            EngineerAfternoon = new Engineer() { DateAdded = DateTime.Now, EngineerId = 10, Name = "Ben" },
            DateAdded = new DateTime(2001, 1, 2)
        };

        public static IList<SupportLog> SupportList => new List<SupportLog>
        {
            new SupportLog()
            {
                LogDate = new DateTime(2000, 1, 1),
                EngineerIdMorning = 1,
                EngineerIdAfternoon = 10,
                DateAdded = new DateTime(2000, 1, 1)
            }
        };

        public static IList<SupportLogDto> SupportListDtos => new List<SupportLogDto>
        {
            new SupportLogDto()
            {
                LogDate = new DateTime(2000, 1, 1),
                EngineerIdMorning = 1,
                EngineerNameMorning = "Bill",
                EngineerIdAfternoon = 10,
                EngineerNameAfternoon = "Bret"
            }
        };

        //public static IList<SupportLogDto> SupportListWith2Entries => new List<SupportLogDto>
        //{
        //    new SupportLogDto()
        //    {
        //        LogDate = DateTime.Now.Date,
        //        EngineerIdMorning = 1,
        //        EngineerIdAfternoon = 2,
        //        EngineerNameMorning = "James",
        //        EngineerNameAfternoon = "Isabella"
        //    },
        //    new SupportLogDto()
        //    {
        //        LogDate = DateTime.Now.Date,
        //        EngineerIdMorning = 3,
        //        EngineerIdAfternoon = 4,
        //        EngineerNameMorning = "Joanne",
        //        EngineerNameAfternoon = "Valerie"
        //    }
        //};
    }
}
