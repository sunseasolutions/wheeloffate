﻿using System.Data.Entity;
using Autofac;
using Autofac.Integration.WebApi;
using WheelOfFate.DataAccess;
using System.Reflection;
using System.Web.Http;
using WheelOfFate.Business;
using WheelOfFate.DataAccess.Configuration;

namespace WheelOfFate.Web
{
    public class AutofacWebApiConfig
    {
        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterStuff(new ContainerBuilder()));
        }

        public static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterStuff(ContainerBuilder builder)
        {
            // API Controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            
            // DbContext
            builder.RegisterType<WheelOfFateContext>().As<DbContext>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();

            // Repositories
            builder.RegisterGeneric(typeof(DataAccess<>)).As(typeof(IDataAccess<>)).InstancePerRequest();
            builder.RegisterType<EngineerDataAccess>().As<IEngineerDataAccess>();

            // Services 
            builder.RegisterType<WheelOfFateService>().As<IWheelOfFateService>();

            Container = builder.Build();
            return Container;
        }
    }
}