﻿using System.Web.Mvc;

namespace WheelOfFate.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult WorkLog()
        {
            ViewBag.Title = "Work Log";

            return View();
        }
    }
}
