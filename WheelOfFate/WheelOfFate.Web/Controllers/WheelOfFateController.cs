﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using WheelOfFate.Business;
using WheelOfFate.Domain;
using WheelOfFate.Dto;

namespace WheelOfFate.Web.Controllers
{
    public class WheelOfFateController : ApiController
    {
        private readonly IWheelOfFateService _wheelOfFateService;

        public WheelOfFateController(IWheelOfFateService wheelOfFateService)
        {
            _wheelOfFateService = wheelOfFateService;
        }
        
        /// <summary>
        /// Get Lucky Winners
        /// </summary>
        [HttpGet]
        [Route("api/wheeloffate/luckywinners")]
        public IEnumerable<Engineer> LuckyWinners()
        {
            return _wheelOfFateService.GetLuckyWinners();
        }

        /// <summary>
        /// Get Last 2 weeks support logs
        /// </summary>
        [HttpGet]
        [Route("api/wheeloffate/supportlog")]
        public IEnumerable<SupportLogDto> SupportLog()
        {
            return _wheelOfFateService.GetSupportLog();
        }


        /// <summary>
        /// Get Last 2 weeks support logs
        /// </summary>
        [HttpPost]
        [Route("api/wheeloffate/confirm/{morningEngineerId}/{afternoonEngineerId}/{shiftDate}")]
        public void Confirm(int morningEngineerId, int afternoonEngineerId, DateTime shiftDate)
        {
            _wheelOfFateService.AddNextWinners(morningEngineerId, afternoonEngineerId, shiftDate);
        }

        [HttpDelete]
        [Route("api/wheeloffate/removelast")]
        public void RemoveLast()
        {
            _wheelOfFateService.RemoveLastShift();
        }
    }
}