﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WheelOfFate.DataAccess;
using WheelOfFate.DataAccess.Configuration;

namespace WheelOfFate.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);

            // Configure Autofac (Dependency Injection)
            AutofacWebApiConfig.Initialize(GlobalConfiguration.Configuration);

            // Configure AutoMapper
            AutoMapperDataConfig.Configure();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
