﻿var wheelOfFateApp = angular.module("wheelOfFateApp", []);

wheelOfFateApp.controller("wheelOfFateController", ["$scope", "$http", function ($scope, $http) {

    // Initialise page
    var initializeWheelOfFate = function () {
        $scope.error = null;
        $scope.supportLoading = false;
        $scope.winnersLoading = false;
        $scope.morningWinner = null;
        $scope.afternoonWinner = null;
        $scope.winnersFound = false;
        getSupportLog();    
    }

    // Get Support Log    
    function getSupportLog() {
        $scope.supportLoading = true;
        $http.get("/api/wheeloffate/supportlog").then(function(response) {
                $scope.supportLogs = response.data;

                // get shift date
                var shiftDate = new Date();
                if ($scope.supportLogs.length > 0) {

                    // Convert to javascript date
                    var serverDate = $scope.supportLogs[0].logDate;
                    var day = serverDate.substring(8, 10);
                    var month = serverDate.substring(5, 7) - 1;
                    var year = serverDate.substring(0, 4);
                    shiftDate = new Date(year, month, day);
                }

                // Add 1 day
                shiftDate.setDate(shiftDate.getDate() + 1);

                // Format for API
                $scope.nextShiftDate = shiftDate;

                $scope.supportLoading = false;
            },
            function(response) {
                $scope.error = "Unable to load data: " + response.message;
                $scope.supportLoading = false;
            });
    }

    // Get the lucky winners
    $scope.getRandom2People = function () {
        $scope.winnersLoading = true;
        $http.get("/api/wheeloffate/luckywinners").then(function (response) {

                if (response.data.length === 2) {

                    // Set and show winners
                    $scope.morningWinner = response.data[0];
                    $scope.afternoonWinner = response.data[1];
                    $scope.winnersFound = true;
                } else {
                    $scope.error = response.data.length + " engineers returned";
                }
                $scope.winnersLoading = false;
            },
            function (response) {
                $scope.error = "Unable to load data: " + response.message;
                $scope.winnersLoading = false;
            });
    }

    $scope.confirmWinners = function () {
        $scope.winnersFound = false;
        $scope.winnersLoading = true;

        // Set data for API call
        var engineer1 = $scope.morningWinner.engineerId;
        var engineer2 = $scope.afternoonWinner.engineerId;
        var date = $scope.nextShiftDate.getFullYear() + '-' + ($scope.nextShiftDate.getMonth() + 1) + '-' + $scope.nextShiftDate.getDate();
        
        // Save data
        $http.post("/api/wheeloffate/confirm/" + engineer1 + "/" + engineer2 + "/" + date).then(function () {
                initializeWheelOfFate();
            },
            function (response) {
                $scope.error = "Unable to save data: " + response.message;
                $scope.winnersLoading = false;
            });
    }

    // Remove the last shift from the list
    $scope.removeLastShift = function () {
        $scope.supportLoading = true;
        $http.delete("/api/wheeloffate/removelast").then(function (response) {
                getSupportLog();
            },
            function (response) {
                $scope.error = "Unable to delete data: " + response.message;
                $scope.supportLoading = false;
            });
    }

    initializeWheelOfFate();

}]);